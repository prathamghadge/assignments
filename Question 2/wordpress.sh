#!/bin/bash

dbname="wordpress"
dbuser="wordpress"
dbpass="wordpress"
dbtable="wp_"
root_password="root"

# Install LAMP based on the type of distribution [ubuntu/centOS]
if [ -f "/etc/lsb-release" ] #Ubuntu/Debian based
then
    sudo add-apt-repository 'deb http://archive.ubuntu.com/ubuntu trusty universe';
    apt-get update;
    DEBIAN_FRONTEND=noninteractive apt install -y mysql-server-5.6 libmysqld-dev mysql-client-5.6;
    apt-get -y install apache2 php7.0 libapache2-mod-php7.0 php7.0-mysql php7.0-mcrypt php7.0-curl php7.0-json php7.0-cgi; 
    chmod 775 -R /var/www/;
    printf "<?php\nphpinfo();\n?>" > /var/www/html/info.php;
    service apache2 restart;
    service mysql restart;#
elif [ -f "/etc/redhat-release" ] #CentOS/Red Hat Based
then
    yum -y install httpd php php-mysql php-fpm epel-release libzip;
    curl -O http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm;
    yum -y localinstall mysql-community-release-el7-5.noarch.rpm;
    yum -y install mysql-server mysql-devel;
    chmod 775 -R /var/www/;
    printf "<?php\nphpinfo();\n?>" > /var/www/html/info.php;
    service mysqld restart;
    service httpd restart;
    chkconfig httpd on;
    chkconfig mysqld on;
else
    echo "Unsupported Operating System";
fi

# MySQL Hardening
mysql -e "UPDATE mysql.user SET Password = PASSWORD('$root_password') WHERE User = 'root';"
mysql -e "DELETE FROM mysql.user WHERE User='';"
mysql -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
mysql -e "DROP DATABASE IF EXISTS test;"
mysql -e "FLUSH PRIVILEGES;"

# WordPress Install

#download wordpress
curl -O https://wordpress.org/latest.tar.gz >> /dev/null;

#unzip wordpress
tar -zxf latest.tar.gz -C /var/www/html/ --strip-components 1;

#create wp config
cp /var/www/html/wp-config-sample.php /var/www/html/wp-config.php;

# Create a Database for Wordpress
create_db="CREATE DATABASE $dbname;"
mysql -u root -p$root_password -e "$create_db";

# Set up a Database User for Wordpress
create_user="CREATE USER '$dbuser'@'localhost' IDENTIFIED BY '$dbpass';";
mysql -u root -p$root_password -e "$create_user";

grant_user="GRANT ALL ON $dbname.* TO '$dbuser'@'localhost';";
mysql -u root -p$root_password -e "$grant_user";

#set database details using sed
sed -i "s/database_name_here/$dbname/g" /var/www/html/wp-config.php;
sed -i "s/username_here/$dbuser/g" /var/www/html/wp-config.php;
sed -i "s/password_here/$dbpass/g" /var/www/html/wp-config.php;
sed -i "s/wp_/$dbtable/g" /var/www/html/wp-config.php;

#remove wordpress/ dir
#rmdir wordpress
#remove zip file
if [ -f /var/www/html/index.html ]
rm /var/www/html/index.html;
fi
rm latest.tar.gz;