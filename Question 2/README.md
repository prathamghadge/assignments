		### Install LAMP with Wordpress ###

	Can be installed on a newly created Ubuntu/CentOS VM.

	Usage :

	- Download the script
	- Make it executable ($ chmod +x wordpress.sh)
	- Execute it ($ ./wordpress.sh)
	- Once done, you can access wordpress using : http://<server_ip>
