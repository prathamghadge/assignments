#!/bin/bash

# Namespace: AWS/ApplicationELB
# Metric: RequestCount

# This script would create a cloudwatch alarm that will be triggered if ApplicationELB/RequestCount >= 100
# As an action, it will send notifications to an SNS topic which in turn would trigger a lambda function

#Assuming the Application Load Balancer Name
LBID="TestLB"

#Assuming the IAM Role ARN
iam_role_arn=""

# Create a random string for lambda permissions
random_id="bonus_id"

#Create a Lambda Function

aws lambda create-function \
	--function-name bonus_function \
	--runtime python2.7 \
	--role $iam_role_arn \
	--handler bonus_handler > lambda.json

lambda_arn=$(jq '.FunctionArn' lambda.json | tr -d '"')

# Create an SNS topic and store output as json
aws sns create-topic --name requests_lambda > sns.json
topic_arn=$(jq '.TopicArn' sns.json | tr -d '"') 

# Create Alarm that will trigger sns topic when in "alarm" state
aws cloudwatch put-metric-alarm \
	--alarm-name requests_increase \
	--metric-name RequestCount \
	--namespace AWS/ApplicationELB \
	--period 1 \
	--evaluation-periods 5 \
	--threshold 100 \
	--comparison-operator GreaterThanOrEqualToThreshold \
	--statistic Sum \
	--unit Count \
	--dimensions Name=LoadBalancerName,Value=$LBID \
	--alarm-actions $topic_arn > alarm.json

# Create sns subscription for the lambda function

aws sns subscribe \
	--topic-arn $topic_arn \
	--protocol lambda \
	--notification-endpoint $lambda_arn > subscription.json

subscription_arn=$(jq '.SubscriptionArn' subscription.json | tr -d '"')

# Add the permission to invoke Lambda using the SNS topic

aws lambda add-permission \
	--function-name bonus_function \
	--statement-id $random_id \
	--action lambda:* \
	--principal sns.amazonaws.com \
	--source-arn $topic_arn

