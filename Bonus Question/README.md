Execution Flow:  
1. Create a lambda function  
2. Create an sns topic. Create a subscription for protocol lambda and provide lambda ARN  
3. Create a CloudWatch Alarm based on the AWS ELB RequestCount Metric. It triggers the sns topic when it reaches the alarm state.  
4. Add permissions to lambda to be exectuable by sns. Specify the sns topic ARN for security.  
  
Created the one_time.sh to accomodate one time tasks mentioned above.  
  
Created the bonus_lambda.py to further create a lambda function deployment package.  
  
The scripts are based on various assumptions made based on the requirement provided. Hence, they may not be executable right away.  
