import boto3

def bonus_handler(event, context):

	# Create instances from an AMI. Assuming the AMI[ami_id="ami-abcxyz"] contains the latest version of the Application	
	ec2_client = boto3.client('ec2')

	ec2_response = ec2_client.run_instances(
	    ImageId='ami-abcxyz',
	    InstanceType='t2.micro',
	    KeyName='test_key',
	    MaxCount=1,
	    MinCount=1,
	    SecurityGroupIds=[
	        'sg-xyzabc',
	    ]
	)

	instance_id = ec2_response[Instances][0][InstanceId]


	# Add the newly created instance to an existing ELB[TestLB]
	elb_client = boto3.client('elb')
	
	elb_response = client.register_instances_with_load_balancer(
	    LoadBalancerName='TestLB',
	    Instances=[
	        {
	            'InstanceId': instance_id
	        },
	    ]
	)

	es_client = boto3.client('es')


	# Get current node count of the ES Domain [TestDomain]
	es_response1 = client.describe_elasticsearch_domain(
   		DomainName='TestDomain'
	)

	es_node_count = es_response1[DomainStatus][ElasticsearchClusterConfig][InstanceCount]
	
	# Increment it by 1
	es_new_count = int(es_node_count) + 1

	# Update the ES Domain
	es_response2 = client.update_elasticsearch_domain_config(
	    DomainName='TestDomain',
	    ElasticsearchClusterConfig={
	        'InstanceCount': es_new_count
	    }
	)


	# Resize the mongoDB instance[[instance_id="i-abcdwxyz"] from m4.large to m4.xlarge
	ec2_client1 = boto3.client('ec2')

	my_instance = 'i-abcdwxyz'

	# Stop the instance
	ec2_client1.stop_instances(InstanceIds=[my_instance])
	waiter=ec2_client1.get_waiter('instance_stopped')
	waiter.wait(InstanceIds=[my_instance])

	# Change the instance type
	ec2_client1.modify_instance_attribute(InstanceId=my_instance, Attribute='instanceType', Value='m4.xlarge')

	# Start the instance
	ec2_client1.start_instances(InstanceIds=[my_instance])








